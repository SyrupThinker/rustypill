extern crate tracers;
extern crate serde_json as json;
extern crate toml;
#[macro_use]
extern crate serde_derive;

use std::io::Read;

#[derive(Debug, Deserialize)]
struct Config {
    username: String,
    password: String,
}

fn main() {
    let mut conf_str = String::new();
    std::fs::File::open("config.toml")
        .unwrap()
        .read_to_string(&mut conf_str)
        .expect("Error reading config");
    let config: Config = toml::from_str(&conf_str).expect("Invalid config file");

    let server = tracers::server::Server::new("https://matrix.org".to_string()).unwrap();

    let (token, resp) = server.login(&config.username, &config.password, "Rustypill").unwrap();

    let p = server.sync(&token, "");

    println!("{:#?}", p);
}
