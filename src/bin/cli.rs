#![feature(match_default_bindings)]

extern crate tracers;
use tracers::*;
use tracers::events::{Event, EventTyped, MessageContentTyped};
use tracers::room::Room;

#[macro_use]
extern crate serde_derive;
extern crate toml;

#[macro_use]
extern crate serde_json;

extern crate termion;
use termion::raw::IntoRawMode;
use std::io::Write;
use termion::input::TermRead;
use termion::event::Key;
use termion::color;

extern crate time;

use std::sync::{Arc, Mutex};
use std::io::Read;
use std::error::Error;

extern crate unicode_segmentation;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Debug, Deserialize)]
struct Config {
    username: String,
    password: String,
    default_room: String,
}

fn main() {
    let mut stdout = std::io::stdout().into_raw_mode().unwrap();
    let stdin = std::io::stdin();

    let mut conf_str = String::new();
    std::fs::File::open("config.toml")
        .unwrap()
        .read_to_string(&mut conf_str)
        .expect("Error reading config");
    let config: Config = toml::from_str(&conf_str).expect("Invalid config file");

    let server = server::Server::new("https://matrix.org".to_string()).unwrap();

    let (token, resp) = server.login(&config.username, &config.password, "Rustypill").unwrap();

    let mut command_hist: Vec<String> = Vec::new();
    let mut hist_pos: usize = 0;    
    
    let mut input_b = InputBuffer::new();
    let mut info_b = InfoBar::new();

    let mut current_room: Option<Room> = server.get_room(config.default_room).ok();
    if let Some(ref r) = current_room {
        info_b.set_name(r.id().to_string());
    }

    let message_stream: Arc<Mutex<Vec<Content>>> = Arc::new(Mutex::new(Vec::new()));

    let mes_str_event_lock = message_stream.clone();
    let token_event = token.clone();
    let server_event = server.clone();

    // Event fetch loop
    std::thread::spawn(move || {
        let mut next = String::new();
        
        loop {
            let response = server_event.sync(&token_event, &next);

            let mut mes_str = mes_str_event_lock.lock().expect("Error: Message Stream Mutex poisoned");
            if response.is_ok() {
                // Got events
                let res: tracers::resp::SyncResponse = response.unwrap();

                next = res.next_batch.to_string();

                if let Some(joined_rooms) = res.rooms.join {
                    for (e_room_id, room) in joined_rooms {
                        for event in room.timeline.events.iter() {
                            match event {
                                Event::Known(x) => match x {
                                    EventTyped::RoomMessage { origin_server_ts: ts, sender, event_id,
                                                              room_id, content, .. } => {
                                        let t = time::Timespec::new((ts / 1000.0) as i64, 0);
                                        let message = if let tracers::events::MessageContent::Known(c) = content {
                                            match c {
                                                MessageContentTyped::Text { body } => body.clone(),
                                                _ => "Unhandeled message content".to_string(),
                                            }
                                        } else {
                                            "Message content of unknown type!".to_string()
                                        };

                                        let room = if let Some(r) = room_id {
                                            r.0.clone()
                                        } else {
                                            e_room_id.clone()
                                        };
                                        
                                        mes_str.push(Content::Matrix(Message {
                                            typ: MType::Normal,
                                            sender: sender.0.clone(),
                                            message,
                                            room,
                                            time: time::at(t),
                                            event_id: event_id.0.clone(),
                                        }));

                                    },
                                    _ => mes_str.push(Content::Internal("Received event with unhandeled known type!".to_string())),
                                },
                                Event::Unknown(x) => mes_str.push(Content::Internal(format!("Received event of unknown type"))),
                            }
                        }
                    }
                }
            } else {
                // Got error
                mes_str.push(Content::Internal(response.unwrap_err().description().to_string()));
            }

            drop(mes_str);
            std::thread::sleep(std::time::Duration::from_secs(10));
        }
    });

    let mes_str_disp_lock = message_stream.clone();

    std::thread::spawn(move || {
        let mut last_len = 0;
        
        loop {
            let (_, h) = termion::terminal_size().unwrap_or((20, 10));
            
            let mes_str = mes_str_disp_lock.lock().expect("Error: Message Stream Mutex poisoned");
            let len = mes_str.len();
            if last_len != len {
                last_len = len;
                
                for e in mes_str[len.saturating_sub(h as usize)..].iter() {
                    match e {
                        Content::Internal(s) => println!("{}\r", s),
                        Content::Matrix(m) => {
                            println!("{} in {} at {}: {}\r", m.sender, m.room, m.time.ctime(), m.message.replace("\n", "\n\r"));
                        },
                    }
                }
            }
            
            drop(mes_str);
            std::thread::sleep(std::time::Duration::from_millis(500));
        }
    });

    let mes_str_inp_lock = message_stream.clone();

    for kev in stdin.keys() {
        info_b.del_notification();
        let mut mes_str = mes_str_inp_lock.lock().expect("Error: Message Stream Mutex poisoned");
        
        match kev.unwrap() {
            Key::Up => {
                let len = command_hist.len();
                hist_pos = hist_pos.saturating_add(1);
                if hist_pos > len {
                    hist_pos = len;
                }
                if let Some(cm) = command_hist.get(len - hist_pos) {
                    input_b.set_string(cm);
                }
            },
            Key::Down => {
                let len = command_hist.len();
                hist_pos = hist_pos.saturating_sub(1);
                if let Some(cm) = command_hist.get(len - hist_pos) {
                    input_b.set_string(cm);
                }
                if hist_pos == 0 {
                    input_b.set_string("");
                }
            },
            Key::Left => input_b.move_left(),
            Key::Right => input_b.move_right(),
            Key::Backspace => input_b.backspace(),
            Key::Delete => input_b.delete(),
            Key::Char('\n') => {
                let com = input_b.take_string();
                command_hist.push(com.clone());
                hist_pos = 0;
                match process_input(&com) {
                    Action::Quit => break,
                    Action::Send(x) => if let Some(ref r) = current_room {
                        r.send(&token, tracers::events::EventType("m.room.message".to_string()),
                               &json!({
                                   "msgtype": "m.text",
                                   "body": x
                               }));
                    }
                    Action::Join(x) => {
                        let new_room = server.get_room(x);
                        if let Err(ref e) = new_room {
                            mes_str.push(Content::Internal(e.description().to_string()));
                        } else {
                            current_room = new_room.ok();
                            info_b.set_name(current_room.as_ref().unwrap().id().to_string());
                        }
                    },
                    Action::None => {},
                }
            },
            Key::Ctrl('c') => break,
            Key::Char(c) => input_b.insert(c),
            _ => {}
        }

        info_b.disp(&mut stdout, false);
        input_b.disp(&mut stdout);
        stdout.flush().unwrap();
    }

    server.delete_device(&token, &config.username, &config.password, resp.device_id.as_ref().unwrap()).unwrap();
}

fn process_input(input: &str) -> Action {
    match input {
        "/quit" => Action::Quit,
        x if x.starts_with("/join ") => Action::Join(x.trim_left_matches("/join ").to_string()),
        x if x.starts_with("/") => Action::None,
        x => Action::Send(x.to_string()),
    }
}

enum Action {
    Quit,
    Send(String),
    Join(String),
    None,
}

struct InfoBar {
    buf_name: String,
    room_topic: String,
    notification: String,
    pos: usize,
    forw: bool,
}

impl InfoBar {
    fn new() -> InfoBar {
        InfoBar {
            buf_name: "<null>".to_string(),
            room_topic: String::new(),
            notification: String::new(),
            pos: 0,
            forw: true,
        }
    }

    fn set_name(&mut self, name: String) {
        self.buf_name = name;
    }

    fn set_topic(&mut self, topic: String) {
        self.room_topic = topic;
        self.pos = 0;
        self.forw = true;
    }

    fn new_notification(&mut self, s: String) {
        self.notification = s;
        self.pos = 0;
        self.forw = true;
    }

    fn del_notification(&mut self) {
        if !self.notification.is_empty() {
            self.notification.clear();
            self.pos = 0;
            self.forw = true;
        }
    }

    fn disp(&mut self, stdout: &mut std::io::Stdout, no_tick: bool) {
        let (w, _) = termion::terminal_size().unwrap_or((20, 10));

        let st = self.buf_name.graphemes(true).count() + 3;
        let aw = w.saturating_sub(st as u16) as usize;

        let inp = if self.notification.is_empty() {
            &self.room_topic
        } else {
            &self.notification
        };

        let message = inp.graphemes(true).skip(self.pos).take(aw).collect::<String>().replace('\n', "\\n");

        if !no_tick {
            match self.pos {
                x if x >= inp.graphemes(true).count().saturating_sub(aw) => self.forw = false,
                x if x == 0 => self.forw = true,
                _ => {},
            }
            
            self.pos = if self.forw {
                self.pos.saturating_add(1)
            } else {
                self.pos.saturating_sub(1)
            };
        }
        
        write!(stdout, "{}{}{}{}{} - {}{}{}",
               color::Bg(color::White), color::Fg(color::Black),
               termion::cursor::Goto(1, 0), termion::clear::CurrentLine, self.buf_name, message,
               color::Fg(color::Reset), color::Bg(color::Reset)).unwrap();
    }
}

struct InputBuffer {
    input: String,
    cursor: usize,
}

impl InputBuffer {
    fn new() -> InputBuffer {
        InputBuffer {
            input: String::new(),
            cursor: 0,
        }
    }

    fn take_string(&mut self) -> String {
        let s = self.input.clone();
        self.clear();
        s
    }

    fn set_string(&mut self, s: &str) {
        self.clear();
        for c in s.chars() {
            self.insert(c);
        }
    }

    fn clear(&mut self) {
        self.input.clear();
        self.cursor = 0;
    }
    
    fn insert(&mut self, c: char) {
        self.input.insert(self.cursor, c);
        self.cursor = self.cursor.saturating_add(c.len_utf8());
    }

    fn backspace(&mut self) {
        if self.cursor != 0 {
            self.move_left();
            let _ = self.input.remove(self.cursor);
        }
    }

    fn delete(&mut self) {
        if self.input.len() != 0 && self.input.len() > self.cursor {
            let _ = self.input.remove(self.cursor);
        }
    }

    fn move_left(&mut self) {
        while {
            self.cursor = self.cursor.saturating_sub(1);  
            !self.input.is_char_boundary(self.cursor)
        } {}
    }

    fn move_right(&mut self) {
        while {
            self.cursor = self.cursor.saturating_add(1);
            if self.input.len() < self.cursor {
                self.cursor = self.input.len();
            };
            !self.input.is_char_boundary(self.cursor)
        } {}
    }

    fn disp(&self, stdout: &mut std::io::Stdout) {
        let (w, h) = termion::terminal_size().unwrap_or((20, 10));

        let mut bc = self.cursor;
        let mut c: usize = 0;
        let mut len: usize = 0;
        
        for s in self.input.graphemes(true) { // For the love of everything do this better!
            len = len.saturating_add(1);
            if bc != 0 {
                c = c.saturating_add(1);
                bc = bc.saturating_sub(s.len());
            }
        }
        
        let aw = w.saturating_sub(2);
        
        let (slice_s, slice_e, shown_pos) = if c < (aw / 2) as usize {
            (0, aw as usize, c as u16)
        } else if c > len - (aw / 2) as usize {
            let s = len.saturating_sub(aw as usize) + 1;
            (s, len, (c - s) as u16)
        } else {
            (c - (aw / 2) as usize, (c + (aw / 2) as usize).saturating_sub(1), (aw / 2))
        };

        let shown_slice = self.input.graphemes(true).skip(slice_s).take(slice_e).collect::<String>();
        
        write!(stdout, "{}{}> {}{}", termion::cursor::Goto(1, h), termion::clear::CurrentLine, shown_slice, termion::cursor::Goto(shown_pos + 3, h)).unwrap();
    }
}

enum Content {
    Matrix(Message),
    Internal(String),
}

struct Message {
    typ: MType,
    sender: String,
    message: String,
    room: String,
    time: time::Tm,
    event_id: String,
}

enum MType {
    Normal,
    Silent,
    Important,
    System,
}
